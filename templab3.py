"""
Created on Tue Jan 26 13:03:05 2021
@file templab3.py
@brief This file acts as the user interface for lab3
@author: josephine
"""

import time
import math
import array
import serial
from matplotlib import pyplot as pyp
import keyboard

class UIFSM:
    '''
    @brief UIFSM is a User Interface operating as a finite state machine updating at a given interval
    @details This finite state machine utilizes the following three states: \n
    - An initial state that runs when the task begins and always transitions to the waiting state
    - State 1 which waits for user input and then write the user input to the nucleo using serial communication so that
    the nucelo file can begin waiting for the user to press a button
    - State 2 waits for the data collection to end so that the data can be formatted into a plot
    '''
    
    ## The Initial state
    S0_INIT = 0
    
    ## The Waiting state
    S1_WAIT = 1
    
    ## The collection state
    S2_RESP = 2
    
    ##The plotting state
    S3_PLOT = 3
    
    def __init__(self):
        '''
        Creates the UIFSM class
        
        '''
        
       # ## The interval of time between tasks 
       # self.interval = interval
        
        ## The initial state to be run
        self.state = self.S0_INIT
        
        
        ## The timestamp for the first iteration
      #  self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
       # self.next_time = self.start_time + self.interval
        
        ## Serial communication set up
        self.ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)

        ## The time data array which is defined as 10s of data collected at 10Hz
        self.timedata = array.array('i')
        
        ## The voltage data
        self.voltdata = array.array('i')
        
        ## The list where data will go to be plotted, must be a list to use strip/split
        self.data = []
        
        ## The inputs to be shown to the user
        self.inputs = 'Please input one of the following commands: \n G: Begin Data Collection'
        
        ## This tells us if the FSM is currently collecting data
        self.collecting = False
        
        ## The character to be sent with serial communication
        self.char = None
        
        ## define the key as not pushed
        self.pushed_key = None
        
        ## Allows for the user to press a key to give an input
        keyboard.on_press(self.onkeypress)
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        
     #   self.currtime = time.time()
     #   
     #  if self.nexttime - self.currtime >= 0:
            
        if self.state == self.S0_INIT:
                
                self.transitionTo(self.S1_WAIT)
                
        elif self.state == self.S1_WAIT:
                
                if self.pushed_key == 'G' or 'g':
                    
                    self.char = 'G'
                    
                    self.ser.write(self.char)
                    
                    self.char = None
                    
                    self.transitionTo(self.S2_RESP)
                    
                else:
                    pass
                
            #Run state 2     
        elif self.state == self.S2_RESP:
                
                #Collect the data from the nucleo once it is being sent
                self.collect_data()
                
        elif self.state == self.S3_PLOT:
            
                self.plot_data(self.voltdata)
                
                
    def collect_data(self):
        '''
        Collects the data from the nucleo and plots the data
        '''
        if (len(self.voltdata)) < 100:
            
            #As long as there is still data in the array being sent, keep reading
            while self.ser.in_waiting() != 0:
            
                #append that recieved data from the nucleo to the array while stripping unneccessary fluff
                self.voltdata.append(self.ser.readline().decode('ascii').strip('\r\n'))
                
        elif len(self.voltdata) == 100:
            
            self.transitionTo(self.S3_PLOT)
            
    def plot_data(self, volt):
        
        pyp.plot(self.timedata,self.volt,"g--")
        
        
    def onkeypress(self, thing):
        '''
        Allows the user to press a key to give an input
        '''
        
        self.pushed_key = thing.name