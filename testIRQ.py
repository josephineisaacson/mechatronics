# -*- coding: utf-8 -*-
"""
Created on Sun Mar  7 15:38:51 2021

@author: josephine
"""


import pyb
led = pyb.Pin(pyb.Pin.board.PA5)
button = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)

def my_function(inpin):
    if button.value == 0:
        led.on()
    else:
        led.off()
    

#interrupt request
button.irq(my_function)