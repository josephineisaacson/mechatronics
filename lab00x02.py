"""
@file lab00x02.py
@author: Josephine Isaacson
@brief This file is a game coded in micropython that tests a users response time to an LED turning on
@details This game utilizes interrupts and the nucleo board to create a game in which a light turns on at random intervals between 2-3 seconds and records the users reaction time to the 
"""

#To do:
#Turn this file into a micrpython file which acts as a game that tests a users reaction
#time

#program should:
    #Start
    #wait 2-3 seconds
    #turn LED on
    #immediately start counting in microseconds
    #stop when the user presses the button
    #store that score
    #repeat
    #when the user presses control c
    #stop the game and show the user the average score
    
import utime
import pyb
import random
import micropython


# Set up  all the inputs, pins and timers


## The timer defined by timer channel 2 
tim = pyb.Timer(2, prescaler = 79,period = 0x7FFFFFFF)

## The user button on the nucleo
user_button = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)

## The led object that will be used to turn the LED on the nucleo on and off
nucleo_led = pyb.Pin(pyb.Pin.board.PA5)

## The initial time is 0, this will be used to mark the time the LED turns on in the game
initialtime = 0

## The current time is 0, this will be used to mark the time the user hits the button after the LED turns on
currenttime = 0

## The reaction time of the user is the  difference between the current time and the start
reactiontime = utime.ticks_diff(currenttime, initialtime)


## This indicates that the first run of the game has not occured yet and the user needs the explanation
firstrun = True

## A statement explaining the game to the user
explain_game = 'Hello! \nWhen the LED on the nucleo turns on, hit the blue user button as fast as you can \n \nWhen you are done playing press ctrl-c \n \nThe game will begin in 5 seconds \nGood Luck!'

## The number of rounds in the game
rounds = 0

## A list where all of the reaction times will go to be averaged at the end
reactionlist = []

## The statement when the user doesn't play or loses
loserstatement = 'Too slow!'

## initiate button pressed to be false
buttonpressed = False

# Define the function used in the IRQ to change the state of the button to pressed
def press(inpin):
    '''
    A function that uses an input pin and when told to by the interrupt 
    '''
    # Define the buttonpressed variable as global so its state can be used between the different methods
    global buttonpressed
    
    #make the button pressed true regardless of the previous state
    if buttonpressed != True:
        
        buttonpressed = True
        
    elif buttonpressed == True:
        pass       

# When the user presses the button call pree function automatically        
## The external interrupt catching on a falling edge on pin 13 which is the blue user button using the press function.
interrupt = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING,pyb.Pin.PULL_UP, press)


## Emergency exception buffer
micropython.alloc_emergency_exception_buf(100)

# This loop is what will run the game     
while True:
    
    # Check if this is the first run of the game
    if firstrun == True:
        
        # Ecplain the game to the user
        print(explain_game)
        #Give the user 5 seconds to read the rules
        utime.sleep_ms(5000)
        
        #Set first run to false so that the game may begin
        firstrun = False
        
        #Delay the start of the game by 5s
        utime.sleep_ms(5000)
        
    elif firstrun == False: 
        
        #Game begins
        
        ## The amount of time the LED should sleep for
        delay = random.randrange(2000,3000, 1)
        
        #delay the LED to the length of time specified in delay
        utime.sleep_ms(delay)
        
        
        # If there has been no keyboard interuppt
        try:
            #Turn the LED on
            nucleo_led.high()
            
            #The initial time is right when the LED turns on
            initialtime = tim.counter()
            
            # the user has 1s to respond to the LED turning on
            while reactiontime <= 10000000:   
               
                
                # If the user responded to the LED on
                if buttonpressed == True:
                    
                    # The current time is the time right when the user hits the button
                    currenttime = tim.counter()
                    
                    # Once the user has pressed the button the LED should turn off
                    nucleo_led.low()
                    
                    # calculate the current reaction time
                    reactiontime = utime.ticks_diff(currenttime, initialtime)
                    
                    # Append the reaction time to the reaction time list
                    reactionlist.append(int(reactiontime))
                
                    # Tell the user what their reaction time is
                    # I included this bc my roommate tested the game for me and wanted to be able to beat her own time over and over again
                    print('Your reaction time is: '+ str(reactiontime/1000) +' Milliseconds')
                    
                    # Reset the buttonpressed to false so that when it runs again it doesn't trigger that the button is already pressed
                    buttonpressed = False
                    # Add another round for averaging purposes
                    rounds += 1
                    
                    break
                #If the button has not been pressed
                else: 
                    #If the user didn't hit the button, pass but update the reaction time
                    currenttime = tim.counter()
                    reactiontime = utime.ticks_diff(currenttime, initialtime)
                    pass  
            # If one second has passed
            else:
                #tell them theyre too slow
                print(loserstatement)
                #turn off the LED
                nucleo_led.low()
                #This still counts as a round and WILL mess up your average >:)
                rounds +=1
                #mess up the users average haha
                reactionlist.append(1000)
            # sum the average every turn so that if the keyboard interrupt happens
            ## The average time calculated 
            averagetime = (sum(reactionlist))/rounds  
        
        # If the user presses ctrl-c to end the game
        except KeyboardInterrupt:
            # Tell the user their average score over the number of rounds they completed
            print('Your average reaction time was: '+ str(averagetime) +' Milliseconds over: ' +str(rounds) + ' rounds')
            
            
            
    