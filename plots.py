# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 14:21:51 2021

@author: josephine
"""

import math
from matplotlib import pyplot as pyp
times = [t for t in range (200)]
data = [5 *(1 - math.exp(-t/5)) for t in times]
pyp.plot(times,data,"g--")

pyp.xlabel("time")
pyp.ylabel("counts")