var namespaces_dup =
[
    [ "ADCtest", null, [
      [ "adc", "ADCtest_8py.html#a83aea075894383780524e97e0dbb6323", null ],
      [ "pinA0", "ADCtest_8py.html#a875aec9ad64b4be93c00f89468dca25a", null ],
      [ "val", "ADCtest_8py.html#a738244e26928b5012ae988ff4ea0b9a8", null ]
    ] ],
    [ "Encoder", "namespaceEncoder.html", "namespaceEncoder" ],
    [ "HW00x01", null, [
      [ "GetChange", "HW00x01_8py.html#a9135d5a6cc8f1086767fe735b08af033", null ]
    ] ],
    [ "lab00x01", null, [
      [ "VendingMachine", "classlab00x01_1_1VendingMachine.html", "classlab00x01_1_1VendingMachine" ],
      [ "task", "lab00x01_8py.html#ac48dbcac8a4c1ca7a149f15c4f94a84f", null ]
    ] ],
    [ "lab00x02", null, [
      [ "press", "lab00x02_8py.html#afad3c9b3056077b39c1f02e584ba40b7", null ],
      [ "averagetime", "lab00x02_8py.html#ab27ada91161df4f856800d7c1a278d82", null ],
      [ "buttonpressed", "lab00x02_8py.html#ab2b014cd6178ae85a0d60efff02e0d14", null ],
      [ "currenttime", "lab00x02_8py.html#a96789a4a0a8aaf751bd38046df3c56d1", null ],
      [ "delay", "lab00x02_8py.html#acd4d45816bedae6068b6f8ebf1189f51", null ],
      [ "explain_game", "lab00x02_8py.html#ae3dadd9a6acf3d0bcb46d86fdd0e204d", null ],
      [ "firstrun", "lab00x02_8py.html#a245e55cc427441b88e8cf801be338604", null ],
      [ "initialtime", "lab00x02_8py.html#ab06f66b5f3178e21b2e40d40d2b6e981", null ],
      [ "interrupt", "lab00x02_8py.html#af7a5ce53115506bd26d589e508c12f7c", null ],
      [ "loserstatement", "lab00x02_8py.html#addeb60d31b29c4bc9d0e006c17da3a38", null ],
      [ "nucleo_led", "lab00x02_8py.html#ac54b611a4e3c803fc74a001047953162", null ],
      [ "reactionlist", "lab00x02_8py.html#a9b4042aeb109309f353118f57fc8be62", null ],
      [ "reactiontime", "lab00x02_8py.html#af659fe54b9a329f7fb4586ee4e69f5f1", null ],
      [ "rounds", "lab00x02_8py.html#ae4d1a7ba6a5eb5b0dad5cacc02fc520b", null ],
      [ "tim", "lab00x02_8py.html#a64f3c13744f4744a4854ee991330ba95", null ],
      [ "user_button", "lab00x02_8py.html#abf31a3998b22abb242ca89bb1ed47a5b", null ]
    ] ],
    [ "lab00x03", null, [
      [ "UI", "classlab00x03_1_1UI.html", "classlab00x03_1_1UI" ]
    ] ],
    [ "lab00x03test", null, [
      [ "count_1", "lab00x03test_8py.html#ab1c12f371cd4f4ad663832ee88b3a053", null ],
      [ "adc", "lab00x03test_8py.html#a54039800dd00d8f65980cc30c1dcbe3a", null ],
      [ "buff", "lab00x03test_8py.html#acee4fc0da0656b7cc2b71b3d8718545a", null ],
      [ "extint", "lab00x03test_8py.html#a591a7261bc6275a9ac73530c7da11788", null ],
      [ "pinA0", "lab00x03test_8py.html#a8672b39c1ca8b66e60a38b4db3a09c38", null ],
      [ "pinPC13", "lab00x03test_8py.html#a4d64ad10e3a0e66969e52fd8045a883f", null ],
      [ "tim", "lab00x03test_8py.html#aa028bb6f2a3b86477fedb82fab32f1e3", null ]
    ] ],
    [ "lab4main", null, [
      [ "adc1", "lab4main_8py.html#a87dc6ae28b17b07ba3a6140955ccc1ed", null ],
      [ "address", "lab4main_8py.html#a835a21606477234f092051643abf7cf9", null ],
      [ "ambient", "lab4main_8py.html#a762136809b99d784ac9fbc9e0e0a39d3", null ],
      [ "Ambient_degC", "lab4main_8py.html#ab5e621e61827991ba953abc53cd88f3b", null ],
      [ "curr_time", "lab4main_8py.html#a55fe4661c02cf3315288cb661255cce3", null ],
      [ "i2c", "lab4main_8py.html#a442d96d63a3f3e606e1553312bed4f87", null ],
      [ "mcp", "lab4main_8py.html#af645c155fd48c2cf08dc3ce28f1589bf", null ],
      [ "mcu_vref", "lab4main_8py.html#a1a4bd677a30028603115aacf511f123a", null ],
      [ "next_time", "lab4main_8py.html#acbfaba2284d58214699076ac69d3c0b0", null ],
      [ "start_time", "lab4main_8py.html#a6cae066aa4118751bd80ca451db883ce", null ],
      [ "stm_list", "lab4main_8py.html#aca412a8b590e8f6de60b73632b5d529c", null ],
      [ "STM_Temp_degC", "lab4main_8py.html#acff3297255fcc481065f01b155c357e9", null ]
    ] ],
    [ "lab7", null, [
      [ "touchpanel", "classlab7_1_1touchpanel.html", "classlab7_1_1touchpanel" ],
      [ "length", "lab7_8py.html#af877b0a7ef083b80dddea1dbc56e20f9", null ],
      [ "mode1", "lab7_8py.html#a65ca98aa9a873120ab4ffad46ff497dd", null ],
      [ "mode2", "lab7_8py.html#a8b06a67e61acc429462e2c1b07e82b94", null ],
      [ "pin1", "lab7_8py.html#a9db9a4c63c1f3be69a3ea9ca3fa3dc68", null ],
      [ "pin2", "lab7_8py.html#adfe8fdc340511792a2bdb80b726d0e56", null ],
      [ "pin3", "lab7_8py.html#a9b603ce80b0277d267ca83cd5b38614f", null ],
      [ "pin4", "lab7_8py.html#a052541f24944a26229958cfaa978fafb", null ],
      [ "touch", "lab7_8py.html#a8c4a3e209b625751ebb8e6ce8e50f559", null ],
      [ "width", "lab7_8py.html#a81baf920a3a4c830bf587c240e5c436c", null ],
      [ "xcenter", "lab7_8py.html#a19d58d1e9c895f80600e5ca216798770", null ],
      [ "ycenter", "lab7_8py.html#a559d2a5927d00798809f0670362033dd", null ]
    ] ],
    [ "lab7adctest", null, [
      [ "adc", "lab7adctest_8py.html#a73b4b279e14841f636478e950e698a02", null ],
      [ "val", "lab7adctest_8py.html#aad01225941e7dbc7595e322026c6a04d", null ],
      [ "Xm", "lab7adctest_8py.html#a77f71e35ef1cd270f67ae209410e3493", null ],
      [ "Xp", "lab7adctest_8py.html#a1ab05049d494e91dae614c86faa175c9", null ],
      [ "Ym", "lab7adctest_8py.html#a40cba6e69f99a078504e8a70b1c5c10e", null ],
      [ "Yp", "lab7adctest_8py.html#a78daaf097911196e9848041121adc429", null ]
    ] ],
    [ "mcp9808", null, [
      [ "MCP9808", "classmcp9808_1_1MCP9808.html", "classmcp9808_1_1MCP9808" ],
      [ "address", "mcp9808_8py.html#a26ecd512bcf78b4d9a65bee737e9d0c3", null ],
      [ "i2c", "mcp9808_8py.html#ae99e487519ec535e9356de31b7f1dee7", null ],
      [ "mcp", "mcp9808_8py.html#a54383af2693144482bf8fc0aa86883b1", null ]
    ] ],
    [ "MotorDriver", "namespaceMotorDriver.html", "namespaceMotorDriver" ],
    [ "new_lab3", null, [
      [ "collect_data", "new__lab3_8py.html#a72a231239a939e29f1f867c6b4233c3e", null ],
      [ "onkeypress", "new__lab3_8py.html#a388fe94a328a067ab2d1251d6b5eae98", null ],
      [ "print_welcome", "new__lab3_8py.html#ada511f358ad790ace6d75398762c648d", null ],
      [ "char", "new__lab3_8py.html#ada1d8a123c0c7dea765d43df55e3f294", null ],
      [ "pushed_key", "new__lab3_8py.html#a931a27f9fbc380fada14248a1897c140", null ],
      [ "ser", "new__lab3_8py.html#ac69124076d23fa112770e695fc191f1b", null ],
      [ "state", "new__lab3_8py.html#aaf238a725f58a0db8167f2339aae45e4", null ],
      [ "voltdata", "new__lab3_8py.html#aa063fdb6b9103b370635856e7fd07b1c", null ]
    ] ],
    [ "plots", null, [
      [ "data", "plots_8py.html#ae4a55ff257e0bf1eb815583966ba0f45", null ],
      [ "times", "plots_8py.html#a5946ae0a1af8c77aec94f7817ed5d426", null ]
    ] ],
    [ "serial1", null, [
      [ "sendChar", "serial1_8py.html#ab1d76a0c2902fbd456435fccf1142253", null ],
      [ "ser", "serial1_8py.html#a59331a4b6ffd16041bfa88f2d0965507", null ]
    ] ],
    [ "temp termproject", null, [
      [ "Balance", "classtemp_01termproject_1_1Balance.html", "classtemp_01termproject_1_1Balance" ],
      [ "button", "temp_01termproject_8py.html#ac9eeec9828ed4373b8755b3bee0bfad5", null ],
      [ "control", "temp_01termproject_8py.html#a16e9c5befdfe1e073793e9e2c8f066b9", null ],
      [ "cx", "temp_01termproject_8py.html#a53b6964f64544a0e8dc32d1e70a3de73", null ],
      [ "cy", "temp_01termproject_8py.html#a1c3ff6738a2224a12c6a9e1c4ba7e3f6", null ],
      [ "enc", "temp_01termproject_8py.html#a173dbeb58092be6922d5ee1241cfb70a", null ],
      [ "enc2", "temp_01termproject_8py.html#a083e63b7088d1e35d7c2278f3688266d", null ],
      [ "interval", "temp_01termproject_8py.html#a479fc7fbea4c6de149b3be80fef69692", null ],
      [ "Lx", "temp_01termproject_8py.html#a488e2715b4fc7cb81824a1db4c7ff905", null ],
      [ "Ly", "temp_01termproject_8py.html#a767f1d7187712a1414553200755fb07b", null ],
      [ "moe", "temp_01termproject_8py.html#ae7ed311677e709cf9a61f8bc2afc5db6", null ],
      [ "moe2", "temp_01termproject_8py.html#ac6cefc6a835848707739df9ad25efbe8", null ],
      [ "pin_IN1", "temp_01termproject_8py.html#a44cc6d4cae78993c94f0fdd528ad3f7b", null ],
      [ "pin_IN2", "temp_01termproject_8py.html#a501beeeefb6a13bf92f3512e53418b30", null ],
      [ "pin_IN3", "temp_01termproject_8py.html#ac0ac813f5abf77fa66f5e78f57331853", null ],
      [ "pin_IN4", "temp_01termproject_8py.html#ade351bb425f7da392f4806dc5f039028", null ],
      [ "pin_nFAULT", "temp_01termproject_8py.html#a29658ef30baa492bf1c0332df2a8dcfa", null ],
      [ "pin_nSLEEP", "temp_01termproject_8py.html#a0a5aa2e691ffb9e450106f5b35f191aa", null ],
      [ "pinA", "temp_01termproject_8py.html#aeb889343fb3b11f716b8c1a11b06e174", null ],
      [ "pinA2", "temp_01termproject_8py.html#a1f04b81c1d2b63f4b5dc11c21577f193", null ],
      [ "pinB", "temp_01termproject_8py.html#a6dac6922d9dab9c5794cb3f5d83882f2", null ],
      [ "pinB2", "temp_01termproject_8py.html#a17d67d7c96a062c4fa95acbb10ce7295", null ],
      [ "tch", "temp_01termproject_8py.html#acf33d7da6967df3683886feeb04f17b3", null ],
      [ "tim", "temp_01termproject_8py.html#a4ac1b9bcc14d24d45086e7e5ae7a4040", null ],
      [ "timer", "temp_01termproject_8py.html#aefd274fa6aee0274c40ff2d9bc829d00", null ],
      [ "timer2", "temp_01termproject_8py.html#ad858559aa81409d3e97644d66e00a00c", null ],
      [ "xm", "temp_01termproject_8py.html#a64dcfe5fddea51e7189f10b844ed2b5e", null ],
      [ "xp", "temp_01termproject_8py.html#a54d8c75a604f53ab104717a1ac1cc1d1", null ],
      [ "ym", "temp_01termproject_8py.html#af77d5080f1f0c3de758e6cad1b618aa7", null ],
      [ "yp", "temp_01termproject_8py.html#a181ec19adea428d6721b7dec9de54d32", null ]
    ] ],
    [ "templab3", null, [
      [ "UIFSM", "classtemplab3_1_1UIFSM.html", "classtemplab3_1_1UIFSM" ]
    ] ],
    [ "testIRQ", null, [
      [ "my_function", "testIRQ_8py.html#a2c263091a267e58996db88fa8146f89e", null ],
      [ "button", "testIRQ_8py.html#a9de3b39700f7517aa3f2dcb68dc4e8dc", null ],
      [ "led", "testIRQ_8py.html#adb3f3664d3a968ded45f8aced95a2ee7", null ]
    ] ],
    [ "uart", null, [
      [ "myuart", "uart_8py.html#acfc8d0619fdb7d9a61189d2cd88b0387", null ],
      [ "val", "uart_8py.html#a47e3e2b316138ec6aaf7a17f56bd24b4", null ]
    ] ],
    [ "untitled6", null, [
      [ "count_isr", "untitled6_8py.html#a25ed9030e58ff3a24bb2284bcf3f25ed", null ],
      [ "extint", "untitled6_8py.html#a6b05e177c849109d32c3889725972685", null ],
      [ "isr_count", "untitled6_8py.html#a6719d5b9f419c21baacf7a2fba8c3158", null ]
    ] ]
];