var lab00x02_8py =
[
    [ "press", "lab00x02_8py.html#afad3c9b3056077b39c1f02e584ba40b7", null ],
    [ "averagetime", "lab00x02_8py.html#ab27ada91161df4f856800d7c1a278d82", null ],
    [ "buttonpressed", "lab00x02_8py.html#ab2b014cd6178ae85a0d60efff02e0d14", null ],
    [ "currenttime", "lab00x02_8py.html#a96789a4a0a8aaf751bd38046df3c56d1", null ],
    [ "delay", "lab00x02_8py.html#acd4d45816bedae6068b6f8ebf1189f51", null ],
    [ "explain_game", "lab00x02_8py.html#ae3dadd9a6acf3d0bcb46d86fdd0e204d", null ],
    [ "firstrun", "lab00x02_8py.html#a245e55cc427441b88e8cf801be338604", null ],
    [ "initialtime", "lab00x02_8py.html#ab06f66b5f3178e21b2e40d40d2b6e981", null ],
    [ "interrupt", "lab00x02_8py.html#af7a5ce53115506bd26d589e508c12f7c", null ],
    [ "loserstatement", "lab00x02_8py.html#addeb60d31b29c4bc9d0e006c17da3a38", null ],
    [ "nucleo_led", "lab00x02_8py.html#ac54b611a4e3c803fc74a001047953162", null ],
    [ "reactionlist", "lab00x02_8py.html#a9b4042aeb109309f353118f57fc8be62", null ],
    [ "reactiontime", "lab00x02_8py.html#af659fe54b9a329f7fb4586ee4e69f5f1", null ],
    [ "rounds", "lab00x02_8py.html#ae4d1a7ba6a5eb5b0dad5cacc02fc520b", null ],
    [ "tim", "lab00x02_8py.html#a64f3c13744f4744a4854ee991330ba95", null ],
    [ "user_button", "lab00x02_8py.html#abf31a3998b22abb242ca89bb1ed47a5b", null ]
];