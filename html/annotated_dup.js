var annotated_dup =
[
    [ "Encoder", "namespaceEncoder.html", [
      [ "Raw_Encoder", "classEncoder_1_1Raw__Encoder.html", "classEncoder_1_1Raw__Encoder" ],
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ],
      [ "Encoder_Interface", "classEncoder_1_1Encoder__Interface.html", "classEncoder_1_1Encoder__Interface" ]
    ] ],
    [ "lab00x01", null, [
      [ "VendingMachine", "classlab00x01_1_1VendingMachine.html", "classlab00x01_1_1VendingMachine" ]
    ] ],
    [ "lab00x03", null, [
      [ "UI", "classlab00x03_1_1UI.html", "classlab00x03_1_1UI" ]
    ] ],
    [ "lab7", null, [
      [ "touchpanel", "classlab7_1_1touchpanel.html", "classlab7_1_1touchpanel" ]
    ] ],
    [ "mcp9808", null, [
      [ "MCP9808", "classmcp9808_1_1MCP9808.html", "classmcp9808_1_1MCP9808" ]
    ] ],
    [ "MotorDriver", "namespaceMotorDriver.html", [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "temp termproject", null, [
      [ "Balance", "classtemp_01termproject_1_1Balance.html", "classtemp_01termproject_1_1Balance" ]
    ] ],
    [ "templab3", null, [
      [ "UIFSM", "classtemplab3_1_1UIFSM.html", "classtemplab3_1_1UIFSM" ]
    ] ]
];