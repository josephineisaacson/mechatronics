"""
Created on Tue Jan 12 12:13:59 2021
@file lab00x01.py
@author: Josephine Isaacson
"""

import math
import keyboard


class VendingMachine:
    '''
    @brief A finite state machine defining the behavior of a soda vending machine
    '''
    ## Constant defining state 0 - initialization
    S0_INIT = 0

    ## Constant defining state 1 - Display
    S1_DISP = 1

    ## Constant defining state 2 - Balance
    S2_BALA = 2

    ## Constant defining state 3 - Eject
    S3_EJEC = 3

    ## Constant defining state 4 - Dispense
    S4_DISP = 4

    def __init__(self):
        '''
        Creates a Vending Machine task
        '''

        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT

        ## A counter of how many times this task has run
        self.runs = 0


        ## Initialize the pushed key to be None
        self.pushed_key = None
        
        ## The initial list for the payment
        self.payment = (0, 0, 0, 0, 0, 0, 0, 0)
        
        ## Allows for the user to press a key to give an input
        keyboard.on_press(self.onkeypress)

    def run(self):
        '''
        Runs one iteration of the task
        '''
        while True:
            #while true bc we don't need to wait in between runs
            if self.state == self.S0_INIT:
                # Print the initial message to the user
                self.printWelcome()
                #transition to state 1
                self.state = self.S1_DISP

            else:
                #all other states begin the same way
                if self.pushed_key == 'e':
                    # empty the payment tuple
                    self.state = self.S3_EJEC
                    #Get the change by emptying the balance
                    change = self.change_getter(self.payment[0]*.01 + self.payment[1]*.05 + self.payment[2]*.1 +
                                                self.payment[3]*.25 + self.payment[4]*1 + self.payment[5] *
                                                5 + self.payment[6]*10 + self.payment[7]*20)
                    #Print the output to the user of how much their change is
                    output = f'Your change is {change[0]} pennies, {change[1]} nickles, {change[2]} dimes, {change[3]} quarters, {change[4]} ones, {change[5]} fives, {change[6]} tens,  {change[7]} twenties'
                    print(output)
                    #reset the pushed key
                    self.pushed_key = None
                #User inserted a penny, add to tuple    
                elif self.pushed_key == '0':
                    #temporary payment list
                    temp = list(self.payment)
                    #add one penny to that spot in the list
                    temp[0] += 1
                    #reset the payment tuple to be the list
                    self.payment = tuple(temp)
                    #reset the key
                    self.pushed_key = None
                # Repeat for a nickel...
                elif self.pushed_key == '1':
                    temp = list(self.payment)
                    temp[1] += 1
                    self.payment = tuple(temp)
                    self.pushed_key = None
                elif self.pushed_key == '2':
                    temp = list(self.payment)
                    temp[2] += 1
                    self.payment = tuple(temp)
                    self.pushed_key = None
                elif self.pushed_key == '3':
                    temp = list(self.payment)
                    temp[3] += 1
                    self.payment = tuple(temp)
                    self.pushed_key = None
                elif self.pushed_key == '4':
                    temp = list(self.payment)
                    temp[4] += 1
                    self.payment = tuple(temp)
                    self.pushed_key = None
                elif self.pushed_key == '5':
                    temp = list(self.payment)
                    temp[5] += 1
                    self.payment = tuple(temp)
                    self.pushed_key = None
                elif self.pushed_key == '6':
                    temp = list(self.payment)
                    temp[6] += 1
                    self.payment = tuple(temp)
                    self.pushed_key = None
                elif self.pushed_key == '7':
                    temp = list(self.payment)
                    temp[7] += 1
                    self.payment = tuple(temp)
                    self.pushed_key = None
                
                #If the user wants a popsi
                elif self.pushed_key == 'p':
                    #Use the balance for the given cost of a popsi
                    self.usebalance('P')
                    #transition to the dispense state
                    self.state = self.S4_DISP
                    #reset pushed key
                    self.pushed_key = None
                #Repeat for a cuke etc
                elif self.pushed_key == 'c':
                    self.usebalance('C')
                    self.state = self.S4_DISP
                    self.pushed_key = None
                elif self.pushed_key == 's':
                    self.usebalance('S')
                    self.state = self.S4_DISP
                    self.pushed_key = None
                elif self.pushed_key == 'd':
                    self.usebalance('D')
                    self.state = self.S4_DISP
                    self.pushed_key = None
                elif self.pushed_key == None:
                    pass



    def onkeypress(self, thing):
        '''
        Allows the user to press a key to give an input
        '''
        
        self.pushed_key = thing.name

    def change_getter(self, payment_value):
        '''
        Calculates the change necessary for a given payment and price
        '''

        task.payment = (5, 0, 2, 0, 0, 0, 0, 0)

        change = [0, 0, 0, 0, 0, 0, 0, 0]
        vals = [0.01, 0.05, 0.1, 0.25, 1, 5, 10, 20]

        i = 7
        while(payment_value > 0 and i > -1):
            if(payment_value / vals[i] >= 1):
                change[i] = math.floor(payment_value/vals[i])
                payment_value = payment_value % vals[i]

            i -= 1

        return change


    def usebalance(self, letter):
        '''
        A function to use the current balance and the chosen beverage to calculate the change given
        '''
        if(letter == 'C'):
            to_pay = 1
        elif(letter == 'P'):
            to_pay = 1.2
        elif(letter == 'S'):
            to_pay = 0.85
        else:
            to_pay = 1.1

        payment_value = self.payment[0]*.01 + self.payment[1]*.05 + self.payment[2]*.1 + \
            self.payment[3]*.25 + self.payment[4]*1 + self.payment[5] * \
            5 + self.payment[6]*10 + self.payment[7]*20 - to_pay

        if(payment_value < 0):
            print("Not enough balance")
        else:
            self.payment = self.change_getter(payment_value)
            if letter == 'C':
                print('Dispensing a Cuke')
            elif letter == 'P':
                print('Dispensing a Popsi')
            elif letter == 'S':
                print('Dispensing a Spryte')
            elif letter == 'D':
                print('Dispensing a Dr.Pupper')
        letter = None

    def printWelcome(self):
        '''
        Prints the welcome message to the user
        '''
        pay = 'Hello! To input a payment: \n 0 = penny \n 1 = nickel \n 2 = dime \n 3 = quarter \n 4 = 1 Dollar \n 5 = 1 five dollar bill \n 6 = 1 ten dollar bill \n 7 = 1 twenty dollar bill'
        select = '\n For cuke press C \n For Popsi press P \n For Dr.Pupper press D \n For Spryte press S'
        eject = '\n Press E to eject'
        print(pay + select + eject)
        return


task = VendingMachine()
task.run()