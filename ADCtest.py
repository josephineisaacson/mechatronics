# -*- coding: utf-8 -*-
"""
Created on Mon Feb  1 10:32:01 2021

@author: josephine
"""
import pyb

## The pin object for pin PA0
pinA0 = pyb.Pin(pyb.Pin.board.PA0)

## An ADC object using pinA0
adc = pyb.ADC(pinA0)

#pyb.ADC.read()

val = adc.read()

print(val)