"""
Created on Tue Jan 26 13:03:50 2021
@file lab00x03Nucleo.py
@brief This file is the file that lives on the nucleo to facilitate lab3
@author: josephine
"""

import utime
import UART
import numpy as np
import pyb





import array


class DataCollectingFSM:
    '''
    @brief The task that collects data from the nucleo when instructed to by the UI
    @details This task will wait for user input via serial communication and begin collecting data for 10 seconds. 5 data points are to be collected every second
    and then the data should be written back to the UI to be plotted.
    '''
    ## The initial state
    S0_INIT = 0
    ## Waiting for an input
    S1_WAIT = 1
    ##Collecting and storing Data
    S2_COLLECT = 2
    ## Sending the data using serial communication
    S3_STOP = 3
    
    def __init__(self):
        
        '''
        Creates the DataCollectingFSM task
        '''
        
        ## The initial state to be run
        self.state = self.S0_INIT
        
        ## The pin object for pin PA0
        self.pinA0 = pyb.Pin(pyb.Pin.cpu.A0)
    
        ## An ADC object using pinA0
        self.pyb.ADC(pinA0)
        
        ## The value for the pin
        self.pinP13 = pyb.Pin(pyb.Pin.board.PC13)
        
        ##The buffer to be used to store samples from the ADC collection
        self.buffy = array.array('H', (0 for index in range(200)))
                
        ## The UART to be used, UART 2
        self.myuart = UART(2)
        
        ## the timer to be used for the ADC running at 10 Hz
        self.tim = pyb.Timer(6, freq = 10)
        
        ## The char that will be read from the buffer
        self.char = None
        
        
    def run(self):
        
        '''
        Runs one iteration of the task
        '''
        
        if self.state == self.S0_INIT:
            
            self.transitionTo(self.S1_WAIT)
            
        elif self.state == self.S1_WAIT:
            
            if self.myuart.any() != 0:
                
                self.char = self.myuart.readchar()
                
                if self.char == 'G':
                    
                    self.transitionTo(self.S2_COLLECT)
                    
                    self.char = None
                    
                else:
                    self.char = None
                    
            else:
                pass
            
        elif self.state == self.S2_COLLECT
        
def count(pin):
    '''
    '''
    
    

    