# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 14:00:37 2021

@author: josephine
"""

import pyb
import array
import micropython

pinA0 = pyb.Pin.board.PA0
buff = array.array('H', (0 for index in range(5000)))
tim = pyb.Timer(2, freq=2*(10**5))
adc = pyb.ADC(pinA0)
pinPC13 = pyb.Pin(pyb.Pin.board.PC13, pyb.Pin.IN)
pinPC13.low()

def count_1(pin):
    '''
    '''
    
    global buff
    adc.read_timed(buff,tim)
    
    
extint = pyb.ExtInt(pinPC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, count_1)

micropython.alloc_emergency_exception_buf(100)