'''
'''
            
import serial
import keyboard
from matplotlib import pyplot as pyp

       
            
class UI:
    '''
    This class behaves as a finite state machine which awaits for user input to begin the nucleo's data collection via serial communication
    '''
 
    
    def __init__():
            
        ## Open the serial port and define the object that we will be using for serial communication
        ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)
        
        ## Allows for the user to press a key to give an input
        keyboard.on_press(onkeypress)
        
        ## The state of the key is not pushed initially
        pushed_key = None
        
        ##An array for the voltage data
        voltdata = array.array('i')
        
        ## The character that will be sent via serial communication
        char = None
        
        ## The list that the data will be entered into, originally empty
        data = []
        
        ## The inital state to run on start
        state = 0
        
        
        
    def run():
        '''
        '''
        
    def onkeypress(thing):
        '''
        Allows the user to press a key to give an input
        '''
        
        pushed_key = thing.name
        