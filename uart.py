# -*- coding: utf-8 -*-
"""
Created on Sun Jan 31 15:52:59 2021

@author: josephine
"""

from pyb import UART

myuart = UART(2)

while True:
    if myuart.any() != 0:
        val = myuart.readchar()
        myuart.write('You sent ASCII ' + str(val) + ' To the Nucleo')
        