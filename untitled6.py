# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 13:50:02 2021

@author: josephine
"""

import pyb


isr_count = 0
def count_isr (which_pin):        # Create an interrupt service routine
    global isr_count
    isr_count += 1
extint = pyb.ExtInt (pyb.Pin.board.PC0,   # Which pin
             pyb.ExtInt.IRQ_RISING,       # Interrupt on rising edge
             pyb.Pin.PULL_UP,             # Activate pullup resistor
             count_isr)                   # Interrupt service routine
# After some events have happened on the pin...
print (isr_count)