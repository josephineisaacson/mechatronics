"""
Created on Tue Mar  9 12:37:59 2021

@author: josephine
"""

# Create a finite state machine in which there are the three following states:
    #wait:
        #wait for the user to press the button
            #when the button is pressed then check if there is contact on the board 
                #no contact, state = controller off, enable motor
                #contact, state  = controller on, enable motor
            
    #controller on:
        #controller is on
        #if there is no more contact with the board state = controller off
        # if fault pin print fault. state = wait
        
    #controller off:
        #controller is off
        #if there is contact with the board state = controller on
        #if the fault pin is true print fault. state = wait