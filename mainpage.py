"""
@file mainpage.py
@mainpage

@section sec_intro Introduction
This website has the documentation for Josephine Isaacson for ME 405 Winter 2021

@page page_lab1 Lab 00x01
@section sec_lab1 Lab 00x01 Source Code
Here is the source code for Lab 00x01:
    https://bitbucket.org/josephineisaacson/mechatronics/src/master/lab00x01.py
    
@section sec_lab1desc Assignment description
The assignment for Lab00x01 was to create a file that acts as a vending machine. The vending machine should do the following:
    - Tell the user how to use the vending machine
    - Take the input payment from the user
    - Dispense the desired beverage and calculate the new balance
    - Eject change at any time the user asks
    .
To do this I used a finite state machine and the keyboard module to take input from the user and functions that calculate and use the balance
To run the vending machine, simply run the file and type in the console.

@page page_lab2button Lab 00x02
@section sec_lab2button Lab 00x02 
@section sec_lab2describe Assignment Description
The assignment for lab00x02 was to create a micropython file that would allow the user to play a game in which:
    - The LED corresponding to pin PA5 turns on on the Nucleo board and remains on for one second
    - The user is to click the blue user button once the LED turns on
    - The reaction time of the user is collected and the game begins again
    - If the user does not react in 1 second, the game tells them that they are too slow
    - When the user presses ctrl-c the game ends and the average time is displayed along with the number of rounds
    .
    
To do this I set up an external interrupt using the pin corresponding to the blue user button so that after the button is pressed and there is a falling edge, the "press" function is called to set the buttonpress variable to be True.
This allows the code to take something that happens physically and allow that condition to be used in loops and in general be translated into software.

To play my timing game, load the file onto the Neuclo, execfile the file, and play until you would like to stop. To stop, press ctrl-c and your average score over the number of rounds will be shown. As you play, your response time will also be shown after each round so you can
try to beat your previous scores!

@section sec_lab00x02source Source Code

The source code is available here:  https://bitbucket.org/josephineisaacson/mechatronics/src/master/lab00x02.py


@page page_lab3press Lab 00x03
@section sec_lab3press Lab00x03 Assignment description

The assignment for lab 3 was to create a collection of files that allowed the user to press 'G' to begin waiting for a button press on the Nucleo.
Once the button is pressed, the nucleo input voltage is collected and stored in an array which is then sent back to the PC
A CSV file is created using the values found by the nucleo amd the times which those values were collected.

The ADC library was used to collect the voltage data.

The source code is available here:
    https://bitbucket.org/josephineisaacson/mechatronics/src/master/templab3.py
    https://bitbucket.org/josephineisaacson/mechatronics/src/master/lab00x03Nucleo.py


@page page_lab4temp Lab 00x04
@section sec_lab4Temperature Assignment description
The assignment for lab00x04 was to create two files that live on the Nucleo that do the following:
    - Facilitate I2C Communication between the nucleo and the mcp9808 sensor
    - Collect temperature data on the Nucleo and on the sensor for 8 hours every 60 seconds
    - Save the data in a CSV file
    .
This lab was done as a partner assignment and I worked with Keanau Robin

To complete this lab we used 2 files: A main file and the mcp9808 driver

The source code is available for the main file:
    
https://bitbucket.org/krrobin/me405_lab4_isaacson_robin/src/master/main.py

The source code is available for the mcp9808 driver is available:

https://bitbucket.org/krrobin/me405_lab4_isaacson_robin/src/master/mcp9808.py    

@section sec_imagelab4 Temperature Plot

Below is the plot of the temperature data that I collected in my bedroom in San Luis Obispo.

I began data collection at 4 am on Tuesday morning because even though I had set the data collection to begin the previous evening, I woke up at 4 to my cat meowing and laying on my keyboard.


He had somehow exited PuTTy. 

The dip in temperatures is most likely due to the fact that I opened my window later in the morning. The Nucleo was sitting on top of my PC which is why it stayed warmer than
most of my room.

@image html lab4tempplot.jpg width=90%

@page page_termproject Term Project Calculations

Here are the hand calcs for Lab00x05 


@image html handcalc1.JPG width=70%
\image html handcalc2.JPG width=70%
\image html handcalc3.JPG width=70%
\image html handcalc4.JPG width=70%
\image html handcalc5.JPG width=70%
\image html handcalc6.JPG width=70%


@page page_lab7 Lab 00x07
@section sec_lab7 Assignment Description

The assignment for lab 7 was to create a file that contained the following methods to be used with the touch panel on the term project hardware:
    - X() which scans the board in the horizontal direction
    - Y() which scans the board in the vertical direction
    - Z() which returns a boolean value indicating whether or not there is contact with the board
    - find_all() which returns the X, Y, and Z values
    .
    
The source code is available:
    
To run the file, simply type execfile('lab7.py') in puTTy
"""