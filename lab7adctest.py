
"""
Created on Mon Mar  8 14:50:02 2021

@author: josephine
"""

import pyb


Yp = pyb.Pin(pyb.Pin.board.PA0, mode=pyb.Pin.IN)

Xm = pyb.Pin(pyb.Pin.board.PA1, mode=pyb.Pin.OUT_PP, value = 0)

Ym = pyb.Pin(pyb.Pin.board.PA7, mode=pyb.Pin.IN)

Xp = pyb.Pin(pyb.Pin.board.PA6, mode=pyb.Pin.OUT_PP, value =1)

#Ym.init(mode = pyb.Pin.ANALOG)

adc = pyb.ADC(Ym)

while True:
    
    val = adc.read()
    print(val) 