# -*- coding: utf-8 -*-
"""
@file lab7.py
@author: josephine
"""

# A constructor that:
    #takes the pins
    #the width and length of the board
    # center coordinate

from pyb import Pin
from pyb import ADC
import utime
    
class touchpanel:
    '''
    @brief A class containing methods that returns the location of the ball and whether or not there is contact with the board
    '''
    
    def __init__(self, pin1, pin2, pin3, pin4, width, length, xcenter, ycenter, mode1, mode2):
        
        '''
        @brief  Creates the touchpanel class
        @param pin1 The pin corresponding to pin Yp
        @param pin2 The pin corresponding to pin Xm
        @param pin3 The pin corresponding to pin Ym
        @param pin4 The pin corresponding to pin Xp
        @param width The width of the touchpad in mm
        @param length The length of the touchpad in mm
        @param xcenter The center of the touchpad in the x direction
        @param ycenter The center of the touchpad in the y direction
        @param mode1 The mode corresponding to an input
        @param mode2 The mode of an output
        '''
        
        # ## The pin corresponding to yp
        # self.pin_yp  = Pin(Pin.board.pin1, mode=mode1)
        
        # ## The pin corresponding to xm which is defined as an output
        # self.pin_xm = Pin(Pin.board.pin2, mode=mode2, value = 0)
        
        # ## The pin corresponding to pin ym, input. To be used with ADC
        # self.pin_ym = Pin(Pin.board.pin3, mode=mode1)
        
        # ## The pin corresponding to pin xp as an output
        # self.pin_xp = Pin(Pin.board.pin4, mode=mode2, value = 1)
        
        ## The pin corresponding to yp
        self.pin_yp  = Pin(pin1)
        
        ## The pin corresponding to xm which is defined as an output
        self.pin_xm = Pin(pin2)
        
        ## The pin corresponding to pin ym, input. To be used with ADC
        self.pin_ym = Pin(pin3)
        
        ## The pin corresponding to pin xp as an output
        self.pin_xp = Pin(pin4)
        
        ## The value for the width of the touch pad in mm
        self.width = width
        
        ## The value for the length of the touch pad in mm
        self.length = length
        

        # ## The ADC object asssociated with pin ym
        # self.adc = ADC(self.pin_ym)
        
        ## The mode for input
        self.inmode = mode1
        
        ## the output mode
        self.outmode = mode2
        

        
    def X_scan(self):
        '''
        @brief This method returns where the contact point is in the X direction
        '''
        # should return a value within the limits of the width and length of the board
        # Should read 0 at the center of the board
        # Account for settling time (background section)
        begin = utime.ticks_us()
        
        self.pin_xp.init(mode = self.outmode, value = 1)
        
        self.pin_xm.init(mode=self.outmode, value = 0)
        
        self.pin_yp.init(mode=self.inmode)
        
        adc = ADC(self.pin_ym)
        
        xval = adc.read()*self.xc
        end = utime.ticks_us()
        print(str(utime.ticks.diff(end,begin)))
        
        return(xval)
    
    def Y_scan(self):
        '''
        @brief This checks where the contact point is in the Y direction
        '''
        begin = utime.ticks_us()
        self.pin_yp.init(mode=self.outmode, value=1)
        self.pin_ym.init(mode=self.outmode, value=0)
        self.pin_xm.init(mode=self.inmode)
        
        adc = ADC(self.pin_xp)
        
        yval = adc.read()*self.yc
        end = utime.ticks_us()
        print(str(utime.ticks.diff(end,begin)))
        return(yval)
    
    def Z_scan(self):
        '''
        @brief This method returns a boolean indicating wether or not there is contact with the touch panel
        '''
        begin = utime.ticks.us()
        self.pin_yp(mode=self.outmode,value = 1)
        self.pin_xm(mode=self.outmode,value=0)
        
        adc = ADC(self.pin_xp)
        
        if adc.read() == 0:
            return(0)
       
        else:
            
            return(1)
        end = utime.ticks.us()
        print(str(utime.ticks.diff(end,begin)))
        
    def find_all(self):
        '''
        @brief This method reads from each of the other three methods and returns the values in a tuple
        '''
        
        return(touchpanel.X_scan(), touchpanel.Y_scan(), touchpanel.Z_scan())
        # reads all three components and returns their values as a tuple
        # read all three channels in less than 500 us
        
        
if __name__ == '__main__':
    
    pin1 = Pin.board.PA7
    
    pin2 = Pin.board.PA0
    
    pin3 = Pin.board.PA1
    
    pin4 = Pin.board.PA6
    
    width = 99.36
    length = 176
    xcenter = width/2
    ycenter = length/2
    
    mode1 = Pin.IN
    mode2 = Pin.OUT_PP
    
    touch = touchpanel(pin1,pin2,pin3,pin4,width,length,xcenter,ycenter,mode1,mode2)
    
    while True:
        touch.find_all()
        
    