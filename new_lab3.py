"""
Created on Tue Feb  2 15:50:57 2021
@author: josephine
"""
import serial
import keyboard
import matplotlib as pyp
import array

## Open the serial port and define the object that we will be using for serial communication
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)
        

        
## The state of the key is not pushed initially
pushed_key = None
        
##An empty array for the voltage data
voltdata = array.array('i')
        
## The character that will be sent via serial communication
char = None
        

##The initial state to run on start
state = 0



def print_welcome():
    '''
    '''
    
    print('Press "G" To begin data collection')
       
def onkeypress(thing):
        '''
        Allows the user to press a key to give an input
        '''
        global pushed_key
        pushed_key = thing.name

## Allows for the user to press a key to give an input
keyboard.on_press(onkeypress)
      
def collect_data():
        '''
        Collects the data from the nucleo and plots the data
        '''
        if (len(voltdata)) < 100:
            
            #As long as there is still data in the array being sent, keep reading
            while ser.in_waiting() != 0:
            
                #append that recieved data from the nucleo to the array while stripping unneccessary fluff
                voltdata.append(ser.readline().decode('ascii').strip('\r\n'))
                
        elif len(voltdata) == 100:
            pass
            
while True:
    
    
    if state == 0:
        
        print_welcome()
        
        state = 1
            
    elif state == 1:
        
        if pushed_key == 'g' or 'G':
            
            char = 'g'
            
            ser.write((char.encode('ascii')))
            
            char = None
            
        else: 
            char = None
            pass
        
    elif state == 2:
        
        collect_data()
        
    elif state ==3:
        ser.close()
        