"""
@file   termproject.py
@author Cameron Ngai, Brennen Irey, Josephine Isaacson
@brief The FSM to run the term project 
"""

# Create a finite state machine in which there are the three following states:
    #wait:
        #wait for the user to press the button
            #when the button is pressed then check if there is contact on the board 
                #no contact, state = controller off, enable motor
                #contact, state  = controller on, enable motor
            
    #controller on:
        #controller is on
        #if there is no more contact with the board state = controller off
        # if fault pin print fault. state = wait
        
    #controller off:
        #controller is off
        #if there is contact with the board state = controller on
        #if the fault pin is true print fault. state = wait
        
    # INTERRUPTS:
        #failure
        #button
        
# Gains:
    #   Note that these assume the position is measured in meters   
    #
    #   Kx = 382.6666
    #   Kt = 612.0
    #   Kdx = 190.666
    #   Ktd = 56.533333
    #
    #   For Table alone...
    #
    #   Kt = 493.333
    #   Kdt = 49.3333 
    #
import pyb
from touch import TCH
from MotorDriver import MotorDriver
import Encoder as en

   # LINES 30 - 65 WAS THE BEGINNING OF SETTING UP THE INTERRUPTS, STILL NEED TO BE MODIFIED AND PRIORITIZED
# ## Initializing the pins for the button 
# button = pyb.Pin(pyb.Pin.board.buttonpin, mode=pyb.Pin.IN)

# ## initiate button pressed to be false
# buttonpressed = False

# def press(inpin):
#         '''
#         A function that, when told to by the interrupt, changes the state of the buttonpressed object 
#         '''
#         # Define the buttonpressed variable as global so its state can be used between the different methods
#         global buttonpressed
    
#         #make the button pressed true regardless of the previous state
#         if buttonpressed != True:
        
#             buttonpressed = True
        
#         elif buttonpressed == True:
#             pass   
# # When the user presses the button call pree function automatically        
# ## The external interrupt catching on a falling edge on pin 13 which is the blue user button using the press function.
# buttoninterrupt = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING,pyb.Pin.PULL_UP, press)
        
# ## Emergency exception buffer
# micropython.alloc_emergency_exception_buf(100)

# def fault (faultpin)
#     '''
#     '''
#     self.state = 
# ## The nFAULT pin
# pin_nFAULT = pyb.Pin(pyb.Pin.board.B2,mode = pyb.Pin.IN)

# faultinterrupt = pyb.ExtInt(pin_nFAULT, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP)


class Balance:
    '''
    @brief This class contains the methods and a finite state machine used to balance a rubber ball on a touch platform
    '''
    
    ## The initial state of the FSM
    S0_INIT = 0
    
    ## The state where the FSM waits for the user to press a button
    S1_WAIT = 1
    
    ## The state in which the controller is on 
    S2_ON = 2
    
    ## The state in which the controller is off
    S3_OFF = 3
    
    # Gains
    Kx = 228*4
    Kt = 493.333*6/11.5
    Kdx = 0*26.66*2/2
    Kdt = 49.333*6/11.5/4
    #   For Table alone...
    #
    Ktt = 493.333*6/11.5/2
    Kdtt = 49.333*6/11.5/4
    
    #touchperiod = 0.001 # Use to make touch sensor not as frequent
    buf_len = 10
    
    def __init__(self,tc,mox,moy,encx,ency,button):
        '''
        @brief Creates the Balance task, takes inputs, and sets up objects to be used
        '''

        ## An object copy of the touch panel class
        self.TCH = tc
        
        ## motor X 
        self.mox = mox
        
        ## Motor Y
        self.moy = moy
        
        ## Encoder x
        self.encx = encx
        
        ## Encoder Y
        self.ency = ency
        
        # initialize the x encoder by setting it to zero
        self.encx.zero()
        
        # initialize the y encoder by zeroing the position
        self.ency.zero()
        
        ## The state to be run on the first iteration of the task
        self.state = self.S0_INIT
        
        ## The current location of the ball in the x direction
        self.bx = self.TCH.X()
        
        ## the current position of the ball in the y direction
        self.by = self.TCH.Y()
        
        self.bvx = 0
        self.bvy = 0
        
        self.bxp = None
        self.byp = None
        self.dt = None
        self.prev_time = None
        self.bx_buf = [0]*self.buf_len
        self.by_buf = [0]*self.buf_len
        #self.bvx_buf = [0]*self.buf_len
        #self.bvy_buf = [0]*self.buf_len
        self.z_buf = [False]*self.buf_len
        
        
        self.z = False
        
        self.PWMx = 0
        self.PWMy = 0
        #self.curr_time = pyb.micros()/10**6
        
        ## The interrupt
        button.irq(self.buttonpress, pyb.Pin.IRQ_FALLING)
        
    def run(self):
        '''
        @brief Runs one iteration of the task
        '''

        #self.curr_time = pyb.micros()/10**6
        
        # if the FSM is in state 0, transition to S1 and track the ball
        if self.state == self.S0_INIT:

            self.state = self.S1_WAIT
            self.trackball()
            
        #elif self.state == self.S1_WAIT:
            
                    
        elif self.state == self.S2_ON:
            #print('on')
            self.onbal()
            if ((not self.mox.safe)|(not self.moy.safe)):
                self.state = self.S1_WAIT
                print('shut down')
            elif not self.z:
                self.state = self.S3_OFF
                print('off')
            
        elif self.state == self.S3_OFF:
            
            #print('off')
            self.offbal()
            if ((not self.mox.safe)|(not self.moy.safe)):
                self.state = self.S1_WAIT
                print('shut down')
            elif self.z:
                self.state = self.S2_ON
                print('on')
                
        self.trackball()
    
    def buttonpress(self, idk):
        '''
        @brief This method is the method to be used in the interrupt
        '''
        if self.state == self.S1_WAIT:
            
            self.encx.zero()
            self.ency.zero()
            self.mox.set_duty(0)
            self.moy.set_duty(0)
            self.mox.clear()
            self.moy.clear()
            
            self.dt = None
            self.prev_time = None
            
            
            #check if the ball is in contact with the board
            if self.z:
                self.state = self.S2_ON
                print('on')
            
            else:
                self.state = self.S3_OFF
                print('off')
        else:
            self.mox.disable()
            self.moy.disable()
            self.state = self.S1_WAIT
            
    def onbal(self):
        self.encx.update()
        self.ency.update()
        
        PWMx = -(self.Kt*self.encx.get_position()+self.Kdt*self.encx.get_vel())-(self.Kx*self.bx+self.Kdx*self.bvx)+15
        PWMy = -(self.Kt*self.ency.get_position()+self.Kdt*self.ency.get_vel())+(self.Kx*self.by+self.Kdx*self.bvy)+20
        
        #print('x')
        #print(PWMx)
        
        #print('y')
        offset = 10
        if PWMx > 0:
            PWMx += offset
        elif PWMx < 0:
            PWMx -= offset
            
        if PWMy > 0:
            PWMy += offset
        elif PWMy < 0:
            PWMy -= offset
            
        self.PWMx = PWMx
        self.PWMy = PWMy
            
        #print('\n')
        self.mox.set_duty(PWMx)
        self.moy.set_duty(PWMy)
        
        print(self.z)
        print(self.by)
        print(PWMy)
        
    def offbal(self):

        self.encx.update()
        self.ency.update()
        
        
        #Lower saturation at 20 PWM
        PWMx = -(self.Ktt*self.encx.get_position()+self.Kdtt*self.encx.get_vel())+7
        PWMy = -(self.Ktt*self.ency.get_position()+self.Kdtt*self.ency.get_vel())+7
        print('kx = ' + str(self.Kx))
        
        
        if PWMx > 0:
            #PWMx *= 1
            PWMx += 20
        elif PWMx < 0:
            PWMx -= 20
            
        if PWMy > 0:
            #PWMy *= 1
            PWMy += 20
        elif PWMy < 0:
            PWMy -= 20
        print('kx = ' + str(self.Kx))
        #self.mox.set_duty(PWMx)
        #self.moy.set_duty(PWMy)
        
        self.mox.set_duty(PWMx)
        self.moy.set_duty(PWMy)
        
        
    def trackball(self):
        '''
        @brief This method tracks the ball on the platform
        @details This method checks if the ball is on the platform and then updates where the ball is on the platform
        '''
        bx = self.TCH.X()
        by = self.TCH.Y()
        z = self.TCH.Z()
        
        #self.bx_buf += [bx]
        #self.by_buf += [by]
        self.z_buf += [z]
        
        self.bx_buf = self.bx_buf[1:]
        self.by_buf = self.by_buf[1:]
        self.z_buf = self.z_buf[1:]
        
        if sum(self.z_buf)/self.buf_len > 0.5:
            self.z = True
        else:
            self.z = False
            
        if (z == self.z)&(z == True):
            self.bx = bx
            self.by = by
        
        #print(str(self.z))
        
            if self.prev_time != None:
                self.dt = pyb.micros()/10**6-self.prev_time
            self.prev_time = pyb.micros()/10**6
        
            if self.dt != None:
                self.bvx = (self.bx-self.bxp)/self.dt
                self.bvy = (self.by-self.byp)/self.dt    
            else:
                self.bvx = 0
                self.bvx = 0
            
        self.bxp = self.bx
        self.byp = self.by
        
        
        
        
        
if __name__ =='__main__':
        
    
    # Motor Setup
    
    # Create the pin objects used for interfacing with the motor drivers      
    pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.A15,pyb.Pin.OUT_PP)   
    pin_nFAULT = pyb.Pin(pyb.Pin.cpu.B2,mode = pyb.Pin.IN) # B2
    pin_IN1     = pyb.Pin(pyb.Pin.cpu.B5)      
    pin_IN2     = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN3     = pyb.Pin(pyb.Pin.cpu.B0)      
    pin_IN4     = pyb.Pin(pyb.Pin.cpu.B1)
    # Create the timer object used for PWM generation
    tim = pyb.Timer(3,freq = 20000);
    # Create motor objects passing in the pins and timer
    moe2     = MotorDriver(pin_nSLEEP, pin_nFAULT,pin_IN1, pin_IN2, tim)
    moe    = MotorDriver(pin_nSLEEP, pin_nFAULT,pin_IN3, pin_IN4, tim)
    
    # Encoder Setup
    ## Timer type for encoder
    timer = 4
    timer2 = 8
    
    ## First encoder input pin
    pinA = pyb.Pin.cpu.B6  # Digital 10
    ## Second encoder input pin
    pinB = pyb.Pin.cpu.B7
    
    ## Second encoder input pin
    pinA2 = pyb.Pin.cpu.C6  # Digital 10
    ## Second encoder input pin
    pinB2 = pyb.Pin.cpu.C7
    
    ## update interval
    interval = 0.001
    
    ## Instance of incoder
    enc2 = en.Encoder(en.Raw_Encoder(timer,pinA,pinB),interval)
    enc = en.Encoder(en.Raw_Encoder(timer2,pinA2,pinB2),interval)
    ## Instance of interface
    
    
    # Touch sensor
    
    ## Positive x pin
    xp = pyb.Pin.cpu.A0
    ## Negative x pin
    xm = pyb.Pin.cpu.A6
    ## Positive y pin
    yp = pyb.Pin.cpu.A7
    ## Negative y pin
    ym = pyb.Pin.cpu.A1
    
    ## Length along x direction
    Lx = 0.17664
    ## Length along y direction
    Ly = 0.09936
    
    ## X origin
    cx = Lx/2
    ## Y origin
    cy = Ly/2
    
    ## TCH object
    tch = TCH(xp,xm,yp,ym,Lx,Ly)
    
    # button
    ## Button
    button = pyb.Pin(pyb.Pin.cpu.C13, mode = pyb.Pin.IN)
    
    control = Balance(tch,moe,moe2,enc,enc2,button)
    control.mox.enable()
    control.moy.enable()
    while True:
        control.run()
     